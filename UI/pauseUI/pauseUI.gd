extends Control


# Declare member variables here. Examples:
# var a = 2
# var b = "text"
onready var mUI = load("res://UI/mainUI/mainUI.tscn").instance()

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass


func _on_close_pressed():
	Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)
	get_tree().paused = false
	queue_free()


func _on_qtd_pressed():
	get_tree().quit()


func _on_qtm_pressed():
	get_tree().get_root().add_child(mUI)
	get_tree().paused = false
	get_tree().get_root().get_child(3).queue_free()
	queue_free()
