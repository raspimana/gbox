extends Control

var spUI = load("res://UI/spUI/spUI.tscn").instance()
var mpUI = load("res://UI/mpUI/mpUI.tscn").instance()


func _on_quit_pressed():
	get_tree().quit()

func _ready():
	Nicense.add_product(Nicense.Product.new("Godot First Person Basic Pickup", [
		Nicense.Copyright.new(
			["2021, CptFurball"],
			"CC0-1.0"
		)
	]))


	Nicense.add_product(Nicense.Product.new("Godot Console", [
		Nicense.Copyright.new(
			["2022, quentincaffeino"],
			"MIT"
		)
	]))

	for argument in OS.get_cmdline_args():
		if argument.find("--console") > -1:
			Console.toggle_console()
func _on_sp_pressed():
	get_tree().get_root().add_child(spUI)
	queue_free()

func _on_mp_pressed():
	get_tree().get_root().add_child(mpUI)
	queue_free()
