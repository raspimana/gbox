extends Control


var thread
var did_thread_run = 0

func _on_LTM_pressed():
	var TM = preload("res://levels/test_map/test_map.tscn").instance()
	thread = Thread.new()
	thread.start(self, "_load_map", TM)


func _load_map(map):
	get_tree().get_root().add_child(map)
	get_tree().get_root().get_node("mpUI").queue_free()
	did_thread_run =+ 1


func _exit_tree():
	if did_thread_run == 1:
		thread.wait_to_finish()


func _on_LPM_pressed():
	var PM = preload("res://levels/proto_01/proto_01.tscn").instance()
	thread = Thread.new()
	thread.start(self, "_load_map", PM)


func _on_back_pressed():
	var mUI = load("res://UI/mainUI/mainUI.tscn").instance()
	get_tree().get_root().add_child(mUI)
	queue_free()
