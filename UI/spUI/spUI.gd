extends Control


var thread
var did_thread_run = 0
onready var map_selector = $VBoxContainer/map_selector
onready var nomap = $nomap
func _on_LTM_pressed():
	thread = Thread.new()
	thread.start(self, "_load_map", preload("res://levels/test_map/test_map.tscn"))


func _load_map(map):
	var map_2_load = map.instance()
	get_tree().get_root().add_child(map_2_load)
	get_tree().get_root().get_node("spUI").queue_free()
	did_thread_run =+ 1


func _exit_tree():
	if did_thread_run == 1:
		thread.wait_to_finish()


func _on_LPM_pressed():
	thread = Thread.new()
	thread.start(self, "_load_map", preload("res://levels/proto_01/proto_01.tscn"))


func _on_back_pressed():
	var mUI = load("res://UI/mainUI/mainUI.tscn").instance()
	get_tree().get_root().add_child(mUI)
	queue_free()


func _on_load_pressed():
	if map_selector.selected == 0:
		nomap.popup()
	else:
		match map_selector.selected:
			1:
				_on_LTM_pressed()
			2:
				_on_LPM_pressed()
