# g%box

s&box clone using Godot, stylized as "g%box".

## Compiling from source

Dependencies: Godot 3.5

1. Clone the repository from git: `git clone https://codeberg.org/raspimana/gbox.git`
2. Open Godot to the cloned directory
3. Go to `Project -> Export`
4. Click `Add` at the top and add your platform
5. Change the export path to desired location
6. Finally, Click `Export All -> Release` & wait.
7. Go to the export path and run the executable!