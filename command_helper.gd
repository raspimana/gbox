extends Node

var spawnUI = load("res://UI/spawnUI/spawnUI.tscn").instance()
#onready var menu = get_tree().get_nodes_in_group("menu")

func _show_spawnUI():
	var sandbox = get_tree().get_nodes_in_group("sandbox")
	if sandbox.empty() == false:
		if is_instance_valid(sandbox[0]) == true:
			get_tree().get_root().add_child(spawnUI)
#			for i in range(sandbox.size()):
#				Console.write(sandbox[i])
#			Console.write(str(is_instance_valid(sandbox)))
			# This is here because godot keeps saying that "p_child is null" if I don't do this. It works, ok?
			spawnUI = load("res://UI/spawnUI/spawnUI.tscn").instance()
	else:
		Console.write("Cannot open spawn UI in main menu!")
#		for i in range(sandbox.size()):
#			Console.write(sandbox[i])
#		Console.write(str(is_instance_valid(sandbox)))
func _set_phys_fps(fps):
		Engine.set_iterations_per_second(fps)

# Called when the node enters the scene tree for the first time.
func _ready():
	Console.add_command('set_phys_fps', self, '_set_phys_fps')\
			.set_description('Sets physics FPS. Recomended is 60+, because under that may jitter.')\
			.add_argument('fps', TYPE_INT)\
			.register()

	Console.add_command('show_spawn_menu', self, '_show_spawnUI')\
			.set_description('Shows the spawn menu.')\
			.register()

#
