extends Spatial
#old stuff for fast close
##-----------------SCENE--SCRIPT------------------#
##    Close your game faster by clicking 'Esc'    #
##   Change mouse mode by clicking 'Shift + F1'   #
##------------------------------------------------#
#
#export var fast_close := true
#
#
#func _ready() -> void:
#	if !OS.is_debug_build():
#		fast_close = false
#
#	if fast_close:
#		print("** Fast Close enabled in the 'L_Main.gd' script **")
#		print("** 'Esc' to close 'Shift + F1' to release mouse **")
#
#

#var PUI = preload("res://levels/pauseUI/pauseUI.tscn").instance()
#var thread
#var didthreadrun = 0


func _loadPUI():
	get_tree().get_root().add_child(preload("res://UI/pauseUI/pauseUI.tscn").instance())
	get_tree().paused = true
#	didthreadrun =+ 1


func _input(event: InputEvent) -> void:
	if event.is_action_pressed("ui_cancel"):
		Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)
		_loadPUI()
#		thread = Thread.new()
#		thread.start(self, "_loadPUI")

	if event.is_action_pressed("mouse_input"):
		match Input.get_mouse_mode():
			Input.MOUSE_MODE_CAPTURED:
				Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)
			Input.MOUSE_MODE_VISIBLE:
				Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)


#func _exit_tree():
#	if didthreadrun == 1:
#		thread.wait_to_finish()
